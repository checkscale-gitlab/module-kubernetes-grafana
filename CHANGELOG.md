## 0.2.0

* feat: Allow for a ldap.toml configuration file

## 0.1.0

* feat: initial release
